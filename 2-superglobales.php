<?php
?>

<form action="" method="post">
    <div>
        <label for="subject">
            Sujet
        </label>
        <input id="subject" name="subject" type="text">
    </div>
    <div>
        <label for="message">
            Un message
        </label>
        <textarea id="message" name="message"></textarea>
    </div>
    <button type="submit">Valider</button>
</form>